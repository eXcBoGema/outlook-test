package OutlookTest;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;

@Sources("classpath:driver-settings.properties")
public interface DriverConfigProperties extends Config {
    @Key("driver-std-wait")
    int stdWait();

    @Key("driver-max-wait")
    int maxWait();
}
