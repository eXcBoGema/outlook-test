package OutlookTest.pages;

import com.codeborne.selenide.ex.ElementNotFound;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.$;

public class LoginPage extends BasePage<LoginPage> {
    public LoginPage() {
        super(byId("idSIButton9"), "https://outlook.live.com/owa/?nlp=1");
    }

    @Step("Login")
    public MainPage login(String email, String password) {
        $(byName("loginfmt")).setValue(email);
        $(byId("idSIButton9")).click();

        $(byName("passwd")).waitUntil(appear, pageLoad.stdWait()).setValue(password);
        $(byId("idSIButton9")).waitUntil(appear, pageLoad.stdWait()).click();

        try {
            $(byId("idSIButton9")).waitUntil(appear, pageLoad.stdWait()).click();
        } catch (ElementNotFound ignored) {}

        return new MainPage().waitWhileLoadingPage(pageLoad.maxWait());
    }
}
