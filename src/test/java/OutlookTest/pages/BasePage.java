package OutlookTest.pages;

import OutlookTest.DriverConfigProperties;
import com.codeborne.selenide.Selenide;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public abstract class BasePage<PAGE extends BasePage<PAGE>> {
    private String url;
    private By pageLoadLocator;
    protected DriverConfigProperties pageLoad;

    protected BasePage(By pageLoadLocator) {
        this.pageLoadLocator = pageLoadLocator;
        pageLoad = ConfigFactory.create(DriverConfigProperties.class);
    }

    protected BasePage(By pageLoadLocator, String url) {
        this(pageLoadLocator);
        this.url = url;
    }

    public PAGE open(String url) {
        Selenide.open(url);
        waitWhileLoadingPage(pageLoad.maxWait());
        return (PAGE) this;
    }

    public PAGE open() {
        return open(url);
    }

    public PAGE refresh() {
        Selenide.refresh();

        try {
            Selenide.switchTo().alert().accept();
        } catch (NoAlertPresentException ignored) { }

        waitWhileLoadingPage(pageLoad.maxWait());
        return (PAGE) this;
    }

    public PAGE waitWhileLoadingPage(long time) {
        $(pageLoadLocator).waitUntil(visible, time);
        return (PAGE) this;
    }
}
