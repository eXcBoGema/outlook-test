package OutlookTest.pages;

import OutlookTest.Email;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.SelenideWait;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Step;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.FluentWait;

import java.util.function.Function;

import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Selectors.byTagName;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class MainPage extends BasePage<MainPage> {
    public MainPage() {
        super(By.xpath("//div[@role='region']/div/div/button/span/span/span"));
    }

    @Step("sendEmail")
    public MainPage sendEmail(String to, String theme, String emailText) {
        $x("//div[@role='region']/div/div/button/span/span/span").click();
        $x("//div[@role='presentation']/div[@role='list']/input").waitUntil(
                appear, pageLoad.stdWait()
        ).setValue(to);
        $x("//div[@role='complementary']//input[@type='text']").setValue(theme);
        $x("//div[@contenteditable]").setValue(emailText);
        $x("//button[@aria-describedby]/span").click();

        return this;
    }

    @Step("getEmail")
    public Email getEmail(String theme) {
        try {
            $x(String.format("//div[contains(@aria-label, '%s')]", theme)).waitUntil(
                    appear, pageLoad.maxWait()
            ).click();
        } catch (TimeoutException ignored) {
            refresh();
        } finally {
            $x(String.format("//div[contains(@aria-label, '%s')]", theme)).waitUntil(
                    appear, pageLoad.maxWait()
            ).click();
        }

        $x("//div[@dir='ltr']/div").waitUntil(appear, pageLoad.stdWait());
        $(By.tagName("li")).waitUntil(Condition.enabled, pageLoad.maxWait());

        FluentWait<WebDriver> hoverEmailWait = new SelenideWait(
                WebDriverRunner.getWebDriver(),
                10000,
                500
        ).ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class);

        return new Email(
                $x("//div[@role='main']//div[@role='heading']/span").getText(),
                $x("//div[@dir='ltr']/div").getText(),
                hoverEmailWait.until(visibilityOfHoverEmail(byTagName("li"))).getText(),
                hoverEmailWait.until(visibilityOfHoverEmail(
                        byXpath("//div[@class='wide-content-host']//div[@role='presentation']/div[@aria-hidden='true']/span")
                )).getText()
        );
    }

    @Step("deleteEmail")
    public MainPage deleteEmail(String theme) {
        $x("//div[@role='menubar']//i[@data-icon-name='Delete']").waitUntil(appear,pageLoad.stdWait()).click();
        return this;
    }
    @Step("openDeletedEmails")
    public MainPage openDeletedEmails() {
        $x("//div[@role='treeitem']/i[@data-icon-name='Delete']").click();
        return this;
    }

    private Function<WebDriver, SelenideElement> visibilityOfHoverEmail(By hoverLocator) {
        String emailFieldLocator = "//div[@data-log-name='Email']/button/div//span";

        try {
            $x(emailFieldLocator).isDisplayed();
            $(By.id("skype")).hover();
        } catch (NoSuchElementException ignored) {}

        return WebDriver -> {
            $(hoverLocator).waitUntil(appear, pageLoad.stdWait()).hover();
            SelenideElement selenideElement = $x(emailFieldLocator);

            return selenideElement.isDisplayed() ? selenideElement : null;
        };
    }
}
