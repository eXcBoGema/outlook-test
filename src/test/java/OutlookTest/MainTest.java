package OutlookTest;

import OutlookTest.pages.LoginPage;
import OutlookTest.pages.MainPage;
import org.aeonbits.owner.ConfigFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class MainTest extends BaseTest {
    private UserInfo user = ConfigFactory.create(User.class).getUserInfo();

    @Test(dataProvider = "emailText",dataProviderClass = MyDataProvider.class)
    public void testSendAndCheckEmail(String theme, String emailText) {
        logger.info("Starting testSendAndCheckEmail...");
        Email email = new LoginPage().open()
                .login(user.getEmail(), user.getPassword())
                .sendEmail(user.getEmail(), theme, emailText)
                .refresh()
                .getEmail(theme);

        new SoftAssert() {{
            assertEquals(email.getTo(), "topelitetester228@outlook.com");
            assertEquals(email.getTheme(), theme);
            assertEquals(email.getText(), emailText);
        }}.assertAll();
    }

    @Test(dataProvider = "emailText", dataProviderClass = MyDataProvider.class)
    public void testSendEmailAndDelete(String theme, String textField) {
        logger.info("Starting testSendEmailAndDelete...");
        MainPage mainPage = new LoginPage().open()
                .login(user.getEmail(), user.getPassword())
                .sendEmail(user.getEmail(), theme, textField)
                .refresh();

        Email sentEmail = mainPage.getEmail(theme);
        Email deletedEmail = mainPage.deleteEmail(theme).openDeletedEmails().getEmail(theme);

        Assert.assertEquals(deletedEmail.getTheme(), sentEmail.getTheme());
        Assert.assertEquals(deletedEmail.getText(), sentEmail.getText());
        Assert.assertEquals(deletedEmail.getTo(), sentEmail.getTo());
        Assert.assertEquals(deletedEmail.getFrom(), sentEmail.getFrom());
    }
}
