package OutlookTest.сonverters;

import OutlookTest.UserInfo;
import org.aeonbits.owner.Converter;

import java.lang.reflect.Method;

public class UserConverterClass implements Converter<UserInfo> {
    @Override
    public UserInfo convert(Method method, String s) {
        String[] separated = s.split(",");
        return new UserInfo(separated[0], separated[1]);
    }
}
