package OutlookTest;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.DataProvider;

public class MyDataProvider {
    @DataProvider(name = "emailText")
    public static Object[][] emailText() {
        return new Object[][]{
                {RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(10)}
        };
    }
}
