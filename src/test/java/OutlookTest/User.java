package OutlookTest;

import OutlookTest.сonverters.UserConverterClass;
import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;

@Sources("classpath:user.properties")
public interface User extends Config {
    @Key("user")
    @ConverterClass(UserConverterClass.class)
    UserInfo getUserInfo();
}