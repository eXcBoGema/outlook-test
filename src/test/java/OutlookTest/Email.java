package OutlookTest;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Email {
    private String theme;
    private String text;
    private String to;
    private String from;
}
