package OutlookTest;

import com.codeborne.selenide.Selenide;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterSuite;

public class BaseTest {
    protected static final Logger logger = LogManager.getLogger();

    @AfterSuite
    public void teardown() {
        Selenide.closeWebDriver();
    }
}
